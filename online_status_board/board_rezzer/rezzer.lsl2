//Space between boxes
float spacing = 1.25; //Spacing of rows cols; X & Z axis
string space_choice_msg = "Please select the spacing between each block, in meters.";
list space_choice_values = ["1.00","1.25","1.50","1.75","2.00","2.25"];
//Rows
string row_choice_msg = "Please select the number of rows desired.";
list row_choice_values = ["1","2","3","4","5","6"]; // For llDialog use
integer num_rows = 1; //Number of rows selected
integer cur_row; //Number of current row (begins @ zero)
//Columns
string col_choice_msg = "Please select the number of columns desired.";
list col_choice_values = ["1","2","3","4","5","6"];
integer num_cols = 1;
integer cur_col;
//Items
string item1 = "HText";
string item2 = "Image";
string item3 = "AdmnBrdRoot";
//Flags
integer haveCol = FALSE;
integer haveRow = FALSE;
integer haveSpace = FALSE;
//Position
vector orig_pos;
float xOrigPos;
float xPos;
float yPos;
float zPos = 15; //To make it always be high enough. 6*2.25m = 13.5m
//Other
integer touchedBefore = FALSE;
integer totalPanels; //Used to help name the root prim
integer chan;
integer onlinePrimName = 1; //Prim number for the rezzed online prims

debug(string msg) {
    llSay(DEBUG_CHANNEL, msg);
}

//Used to dynamically adjust the height of the rezzed panels; no need to have a
// small set all the way up in the air for no reason
updateHeightOffset() {
    zPos = (num_rows * spacing) + 1;
}

//Compact function to put buttons in "correct" human-readable order ~ Redux
list orderButtons(list buttons) {
    return llList2List(buttons, -3, -1) + llList2List(buttons, -6, -4) + llList2List(buttons, -9, -7) + llList2List(buttons, -12, -10);
}

//Movement so we don't have to deal with the 10m limit of llRezObject. Also easier
// than calculating the distance needed to move manually
warpPos( vector destpos ) {  //R&D by Keknehv Psaltery, 05/25/2006
    //with a little poking by Strife, and a bit more
    //some more munging by Talarus Luan
    //Final cleanup by Keknehv Psaltery
    //Changed jump value to 411 (4096 ceiling) by Jesse Barnett
    // Compute the number of jumps necessary
    integer jumps = (integer)(llVecDist(destpos, llGetPos()) / 10.0) + 1;
    // Try and avoid stack/heap collisions
    if (jumps > 411)
        jumps = 411;
    list rules = [ PRIM_POSITION, destpos ];  //The start for the rules list
    integer count = 1;
    while ( ( count = count << 1 ) < jumps)
        rules = (rules=[]) + rules + rules;   //should tighten memory use.
    llSetPrimitiveParams( rules + llList2List( rules, (count - jumps) << 1, count) );
    if ( llVecDist( llGetPos(), destpos ) > .001 ) //Failsafe
        while ( --jumps ) 
            llSetPos( destpos );
 }

default {
    state_entry() {
        //Set channel for dialogs
        chan = llRound(llFrand(1337) + 30);
        //Order the buttons
        space_choice_values = orderButtons(space_choice_values);
        row_choice_values = orderButtons(row_choice_values);
        col_choice_values = orderButtons(col_choice_values);
        
    }

    touch_start(integer qwe) {
        if ( llDetectedKey(0) == llGetOwner() ) {
            //Begin the dialog sequence; if touched while initiated, start it over
            if (touchedBefore) {
                llOwnerSay("Information gathering process has been restarted. Please close any active " +
                    "dialogs by clicking the \"Ignore\" button in the lower right-hand corner of " +
                    "the dialog.");
            }
            else {
                touchedBefore = TRUE;
            }
            haveCol = haveRow = haveSpace = FALSE;
            llListenRemove(chan);
            llListen(chan, "", llGetOwner(), "");
            //Begin with column number choice
            llDialog(llGetOwner(), col_choice_msg, col_choice_values, chan);
        }
        else {
            llInstantMessage(llDetectedKey(0), "Argh! Why did you touch me there?! >=/");
        }
    }
    
    listen( integer channel, string name, key id, string msg ) {
        if (!haveCol) {
            num_cols = (integer)msg;
            haveCol = TRUE;
            llDialog(llGetOwner(), row_choice_msg, row_choice_values, chan);
        }
        else if (!haveRow) {
            num_rows = (integer)msg;
            haveRow = TRUE;
            llDialog(llGetOwner(), space_choice_msg, space_choice_values, chan);
        }
        else if (!haveSpace) {
            spacing = (float)msg;
            haveSpace = TRUE;
            //Done with listens, remove it
            llListenRemove(chan);
            //On to the fun part! 8D
            state process;
        }
    }
}

//This is where the magic happens!
state process {
    state_entry() {
        //Change the height panels are rezzed at based on settings
        updateHeightOffset();
        orig_pos = llGetPos();
        //Used in naming the root of the set when rezzed
        totalPanels = num_cols * num_rows;
        //Initial position calculation
        float xOffset = (0.5 * spacing * num_cols);
        vector tmp = llGetPos() - <xOffset,0,0>;
        xOrigPos = tmp.x;
        xPos = xOrigPos;
        yPos = tmp.y;
        zPos = tmp.z + zPos;
        //Values for the root prim rez
        vector rootPrimPos = <xPos, yPos, zPos> + <-1, 0, 1>;
        //The heavy lifting
        for (cur_row = 0; cur_row < num_rows; cur_row++) {
            for (cur_col = 0; cur_col < num_cols; cur_col++) {
                vector delta = <xPos, yPos, zPos>;
                warpPos(delta);
                llRezObject(item1, llGetPos(), <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, onlinePrimName);
                onlinePrimName++;
                llRezObject(item2, llGetPos(), <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, onlinePrimName);
                onlinePrimName++;
                xPos += spacing;
            }
            xPos = xOrigPos;
            zPos -= spacing;
        }
        //Rez the root prim
        warpPos(rootPrimPos);
        llRezObject(item3, llGetPos(), <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, totalPanels);
        warpPos(orig_pos);
        llOwnerSay("The root prim is next to the first prim rezzed in the set. Please position " +
            "it how you'd like, and link all the prims!");
        llOwnerSay("Rezzing complete! Script is now resetting...");
        llResetScript();
    }
}